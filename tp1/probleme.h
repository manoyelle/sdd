#ifndef LISTE
#include "liste.h"
#endif

#ifndef MATRICE
#include "matrice.h"
#endif

/*------------------------------------------------------------------------------*/
/* RemplirListe           Initialise une liste chainées avec k céllules         */
/*                        et la remplie des K coûts les plus faibles            */
/*                                                                              */
/* en entrée :        k : un entier donnant le nombre de coûts à inserer        */
/*                        dans la liste chainées                                */
/*              matrice : matrice 2 dimensions m,n donnant les coûts            */
/*                        à insérer dans la liste chainées                      */
/*                    m : nombre de lignes de la matrice                        */
/*                    n : nombre de colonnes de la matrice                      */
/*                                                                              */
/* en sortie :  adresse de la tete de la liste chainées remplie                 */
/*------------------------------------------------------------------------------*/
cellule_t * RemplirListe(int K, float **matrice, int m, int n);

/*------------------------------------------------------------------------------*/
/* suppOccurences         Supprime les occurences des coûts d'une usine u dans  */
/*                        une liste chainées                                    */
/*                                                                              */
/* en entrée :     tete : adresse de la tête de la liste chainées               */
/*                    u : l'usine dont on souhaite supprimer les occurences     */
/*------------------------------------------------------------------------------*/
void suppOccurences(cellule_t **tete, int u);