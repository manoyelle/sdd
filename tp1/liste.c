#include <stdio.h>
#include <stdlib.h>
#include "liste.h"

void LibererListe(cellule_t **tete)
{
    cellule_t        * cour = *tete;
    cellule_t        * prec = NULL;

    while(cour != NULL)
    {
        prec = cour;
        cour = cour->suivant;
        free(prec);
    }
}

void AfficheListe(cellule_t *tete)
{
    cellule_t        * cour = tete->suivant;
    int                cpt=0;

    printf("affichage de la liste :\n");
    while(cour != NULL)
    {
    	cpt++;

        printf("cellule %2d : cout->%5.2f i(Usine)->%d j(Periode)->%d\n",cpt,cour->cout,cour->usine,cour->periode);
        cour = cour->suivant;
    }
    printf("\n");
}

void ecrireListe(FILE *f, cellule_t *tete)
{
    cellule_t        * cour = tete->suivant;
    int                cpt=0;

    fprintf(f,"affichage de la liste :\n");
    while(cour != NULL)
    {
    	cpt++;

        fprintf(f,"cellule %2d : cout->%5.2f i(Usine)->%d j(Periode)->%d\n",cpt,cour->cout,cour->usine,cour->periode);
        cour = cour->suivant;
    }
    fprintf(f,"\n");
}

cellule_t * AllocationMaillon(float c, int i, int j)
{
    cellule_t 		 * elt = (cellule_t*) malloc(sizeof(cellule_t));
    if (elt != NULL)
    {
        elt->cout = c;
        elt->usine = i;
        elt->periode = j;
        elt->suivant = NULL;
    }
    return elt;
}

void ADJ_CEL(cellule_t **prec, cellule_t *elt)
{
	elt->suivant = *prec;
	*prec = elt;
}

void SUP_CEL(cellule_t **prec)
{
    cellule_t 		 * temp = *prec;
    *prec = temp->suivant;
    free(temp);
}

void InsereTrie(cellule_t **tete, cellule_t *elt)
{
    cellule_t ** prec = rechPrecTrie(tete,elt->cout);

    ADJ_CEL(prec, elt);
}

cellule_t ** rechPrecTrie(cellule_t **tete, float v) 
{
    cellule_t 		 * cour = *tete;
    cellule_t 		** prec = tete;
    while((cour != NULL)&&(cour->cout < v))
    {
    	prec = &cour->suivant;
        cour = cour->suivant;
    }
    return prec;
}

cellule_t ** rechPrec(cellule_t **tete, int v) 
{
    cellule_t 		 * cour = *tete;
    cellule_t 		** prec = tete;
    while((cour != NULL)&&(cour->usine != v))
    {
    	prec = &(cour->suivant);
        cour = cour->suivant;
    }

    if (cour == NULL)
    {
        prec = NULL;
    }

    return prec;
}