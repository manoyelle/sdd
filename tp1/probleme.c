#include <stdio.h>
#include <stdlib.h>
#include "matrice.h"
#include "liste.h"
#include "probleme.h"

cellule_t * RemplirListe(int K, float **matrice, int m, int n)
{
    int                i = 0, j = 0, OK = 1;
    float              max =0;
    cellule_t        * tete = NULL;

    tete = AllocationMaillon(-1,-1,-1);
    if(tete != NULL)
    {
        while((i < m)&&(OK == 1))
        {
            j = 0;
            while((j < n)&&(OK == 1))
            {
                if( ((i*n)+j) < K)
                {   
                    cellule_t * elt = AllocationMaillon(matrice[i][j],i+1,j+1);
                    if (elt != NULL) 
                    {
                        InsereTrie(&tete,elt);
                        if(matrice[i][j] > max)
                        {
                            max = matrice[i][j];
                        }
                    }
                    else
                    {
                        OK = 0;
                    }

                }
                else if(matrice[i][j] < max)
                {   
                    cellule_t * elt = AllocationMaillon(matrice[i][j],i+1,j+1);
                    if (elt != NULL) 
                    {
                        InsereTrie(&tete,elt);
                        cellule_t * cour = tete;
                        cellule_t * prec = NULL;
                        while (cour->suivant != NULL)
                        {
                            prec = cour;
                            cour = cour->suivant;
                        }
                        SUP_CEL(&prec->suivant);
                    }
                    else 
                    {
                        OK = 0;
                    }
                }
                j++;
            }
            i++;
        }

        if(OK == 0)
        {
            LibererListe(&tete);
        }
    }

    return tete;
}

void suppOccurences(cellule_t **tete, int u)
{
    cellule_t 		** prec = tete;

    printf("Suppression des occurences Usine = %d\n\n",u);
    while (prec != NULL)
    {
        prec = rechPrec(prec,u);
        if (prec != NULL)
        {
            SUP_CEL(prec);
        }
    }
}