#include <stdio.h>
#include <stdlib.h>
#include "matrice.h"
#include "liste.h"
#include "probleme.h"

int main(int argc, char ** argv)
{
    int                m = 0, n = 0, K = 0, u = 0, numFich = 0;
    char               nomFich[16];
    float           ** matrice = NULL;
    FILE             * fichierLecture = NULL, * fichierEcriture = NULL;
    cellule_t        * liste = NULL;

    if(argc >= 4)
    {
    	numFich = atoi(argv[1]);
        K = atoi(argv[2]);
        u = atoi(argv[3]);

        if (numFich > 0 && numFich < 100)
        {
            sprintf(nomFich,"test%d.txt",numFich);
            fichierLecture = fopen(nomFich,"r");

            if(fichierLecture != NULL)
            {
                sprintf(nomFich,"test%d_res.txt",numFich);
                fichierEcriture = fopen(nomFich,"w");

                if(fichierEcriture != NULL)
                {
                	fprintf(fichierEcriture,"Parametres d'entree :\nK = %d\nUsine choisit = %d\n\n",K,u);
                	matrice = AllocationMatrice(fichierLecture,&m,&n);

	                if(matrice != NULL)
	                {
	                    AfficheMatrice(matrice,m,n);
	                    ecrireMatrice(fichierEcriture,matrice,m,n);

	                    liste = RemplirListe(K,matrice,m,n);
	                    LibererMatrice(matrice,m);

	                    if(liste != NULL)
	                    {
	                        AfficheListe(liste);
	                        ecrireListe(fichierEcriture,liste);
	                        suppOccurences(&liste,u);
                            fprintf(fichierEcriture,"Suppression des occurences Usine = %d\n\n",u);
	                        AfficheListe(liste);
	                        ecrireListe(fichierEcriture,liste);
	                        LibererListe(&liste);
	                    }
	                }
	                fclose(fichierEcriture);
                }
                fclose(fichierLecture);
            }
        }
    }
    
    return 0;
}