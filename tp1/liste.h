#ifndef LISTE
#define LISTE


typedef struct cellule cellule_t;
struct cellule
{
    float              cout;
    int                usine,periode;
    struct cellule   * suivant;
};

/*------------------------------------------------------------------------------*/
/*AllocationMaillon     Alloue un maillon et initialise ses champs              */
/*                                                                              */
/*en entée :        c : valeur à mettre dans le champ cout                      */
/*                  i : valeur à mettre dans le champ usine                     */
/*                  j : valeur à mettre dans le champ periode                   */
/*                                                                              */
/*en sortie :  adresse du maillon si l'alocation c'est bien passée, NULL sinon  */
/*------------------------------------------------------------------------------*/
cellule_t * AllocationMaillon(float c, int i, int j);

/*------------------------------------------------------------------------------*/
/* ADJ_CEL              Ajoute une céllule dans une liste chainées              */
/*                      après le précédent                                      */
/*                                                                              */
/* en entrée :   prec : l'adresse de la céllule précédent la nouvelle céllule   */
/*                elt : la nouvelle céllule inséré dans la liste                */
/*------------------------------------------------------------------------------*/
void ADJ_CEL(cellule_t **prec, cellule_t *elt);

/*------------------------------------------------------------------------------*/
/* SUP_CEL             Supprime une céllule dans une liste chainées             */
/*                     après le précédent                                       */
/*                                                                              */
/* en entrée :  prec : l'adresse de la céllule précédent la célulle             */
/*                     qui doit être supprimé                                   */
/*------------------------------------------------------------------------------*/
void SUP_CEL(cellule_t **prec);

/*------------------------------------------------------------------------------*/
/* LibererListe        Libère la liste chainée                                  */
/*                                                                              */
/* en entrée :  tete : adresse de la tete de la liste chainée à libérer         */
/*------------------------------------------------------------------------------*/
void LibererListe(cellule_t **tete);

/*------------------------------------------------------------------------------*/
/* InsereTrie             Insère un élément dans une liste triée                */
/*                                                                              */
/* en entrée :  tete : adresse de la tete de la liste                           */
/*               elt : élément à insérer                                        */
/*------------------------------------------------------------------------------*/
void InsereTrie(cellule_t **tete, cellule_t *elt);

/*------------------------------------------------------------------------------*/
/* AfficheListe        Affiche le contenu de la liste de manière formatée       */
/*                                                                              */
/* en entrée :  tete : tete de la liste à afficher                              */
/*------------------------------------------------------------------------------*/
void AfficheListe(cellule_t *tete);

/*------------------------------------------------------------------------------*/
/* ecrireListe         Ecrit le contenu de la liste de manière formatée         */
/*                     dans un fichier                                          */
/*                                                                              */
/* en entrée :  tete : tete de la liste à afficher                              */
/*------------------------------------------------------------------------------*/
void ecrireListe(FILE *f, cellule_t *tete);

/*------------------------------------------------------------------------------*/
/* rechPrecTrie        Recherche le précédent de la première occurence de v     */
/*                     dans une liste triée                                     */
/*                                                                              */
/* en entrée :  tete : adresse de la tete de la liste                           */
/*                 v : valeur recherchée (cout)                                 */
/*                                                                              */
/* en sortie :  l'adresse du champ suivant du maillon précédent le premier de   */
/*              cout v s'il existe, NULL sinon                                  */
/*------------------------------------------------------------------------------*/
cellule_t ** rechPrecTrie(cellule_t **tete, float v);

/*------------------------------------------------------------------------------*/
/* rechPrec            Recherche le précédent de la première occurence de v     */
/*                     dans une liste                                           */
/*                                                                              */
/* en entrée :  tete : adresse de la tete de la liste                           */
/*                 v : valeur recherchée (usine)                                */
/*                                                                              */
/* en sortie :  l'adresse du champ suivant du maillon précédent le premier      */
/*              d'usine v s'il existe, NULL sinon                               */
/*------------------------------------------------------------------------------*/
cellule_t ** rechPrec(cellule_t **tete, int v);

#endif