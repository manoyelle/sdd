#ifndef PROBLEME
#define PROBLEME

#include "arbre.h"
#include "pile.h"

#define PILEMAX 30
/*---------------------------------------------------------------------------------*/
/* Construction     Constuit un abre à partir de la                                */
/*                  notation algébrique contenu dans un fichier                    */
/*                                                                                 */
/* entrée :         fichier : pointeur sur un fichier contenant                    */
/*                            la notation algébrique de l'arbre                    */
/*                                                                                 */
/* sortie :         tete de l'arbre construit, NULL si arbre vide                  */
/*---------------------------------------------------------------------------------*/
cellule_t * Construction(FILE *fichier);

/*---------------------------------------------------------------------------------*/
/* Affichage        Affiche un arbre précédé d'une chaine                          */
/*                                                                                 */
/* entrée :            tete : adresse de la tête de l'arbre                        */
/*                   chaine : chaine à préfixer avant les mots à affichés          */
/*                        i : nombre de caractères de chaine                       */
/*---------------------------------------------------------------------------------*/
void Affichage(cellule_t **tete,char *chaine, int i);

/*---------------------------------------------------------------------------------*/
/* Insertion        Insère un mot dans un l'arbre,                                 */
/*                  ne fait rien si celui-ci est déjà présent                      */
/*                                                                                 */
/* entrée :            tete : adresse de la tête de l'arbre                        */
/*                      mot : chaine à insérer dans l'arbre                        */
/*---------------------------------------------------------------------------------*/
void Insertion(cellule_t **tete, char *mot);

/*---------------------------------------------------------------------------------*/
/* RechercheMotif   Affiche les mots de l'arbre commençant par un motif donné      */
/*                                                                                 */
/* entrée :            tete : adresse de la tête de l'arbre                        */
/*                    motif : chaine à insérer dans l'arbre                        */
/*---------------------------------------------------------------------------------*/
void RechercheMotif(cellule_t **tete, char *motif);

#endif