#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "probleme.h"

cellule_t * Construction(FILE *fichier)
{
    cellule_t        * tete = NULL;
    cellule_t       ** prec = &tete;
    cellule_t        * cour = NULL;
    pileTete_t       * pile = initPile(PILEMAX);
    char               c='\0';

    if(pile != NULL)
    {
        while(!feof(fichier))
        {
            fscanf(fichier,"%c",&c);
            if(c == '(')
            {
                fscanf(fichier,"%c",&c);
                cour = AllocationMaillon(c);
                empiler(&pile, cour);
                *prec = cour;
            }
            else if(c == '*')
            {
                prec = &(cour->fils);
            }
            else if(c == ')')
            {
                depiler(&pile, &cour);
            }
            else if(c == '+')
            {
                depiler(&pile, &cour);
                prec = &(cour->frere);
                fscanf(fichier,"%c",&c);
                cour = AllocationMaillon(c);
                empiler(&pile, cour);
                *prec = cour;
            }
            else if(((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')))
            {
                cour = AllocationMaillon(c);
                *prec = cour;
            }
        }
        libererPile(&pile);
    }

    return tete;
}

void Affichage(cellule_t **tete,char *chaine, int i)
{
    cellule_t        * cour = *tete;
    pileTete_t       * pile = initPile(PILEMAX);

    if(pile != NULL)
    {
        while(cour != NULL)
        {
            if((cour->lettre >= 'A') && (cour->lettre <= 'Z'))
            {
                chaine[i] = (cour->lettre) + 32;
                printf("%s\n",chaine);
            }
            else
            {
                chaine[i] = cour->lettre;
            }
            empiler(&pile, cour);
            cour = cour->fils;
            while((cour == NULL) && (!pileVide(pile)))
            {
                depiler(&pile, &cour);
                cour = cour->frere;
                chaine[i] = '\0';
                i--;
            }
            i++;
        }
        libererPile(&pile);
    }
}

void Insertion(cellule_t **tete, char *mot)
{
    cellule_t       ** prec = tete;
    cellule_t        * elt = NULL;
    int                i = 0, continuer = 1;

    if(mot[0] != '\0')
    {
        while(continuer)
        {
            prec = rechPrecTrie(prec, mot[i]);;
            if(((*prec) == NULL) || (mot[i] != tolower((*prec)->lettre)))
            {
                continuer = 0;
                elt = AllocationMaillon(mot[i]);
                elt->frere = (*prec);
                (*prec) = elt;
                i++;  
                while(mot[i] != '\0') 
                {
                    elt = AllocationMaillon(mot[i]);
                    (*prec)->fils = elt;
                    prec = &((*prec)->fils);
                    i++;
                }
            }
            else
            {
                i++;
                if(mot[i] != '\0')
                {
                    prec = &((*prec)->fils);
                }
                else
                {
                    continuer = 0;
                }
            }
        }  
        (*prec)->lettre = toupper((*prec)->lettre);
    }
}

void RechercheMotif(cellule_t **tete, char *motif)
{
    cellule_t       ** prec = tete;
    int                i = 0, continuer = 1, maj = 0;

    while((motif[i] != '\0') && (continuer))
    {
        prec = rechPrecTrie(prec, motif[i]);
        if(((*prec) != NULL) && (tolower((*prec)->lettre) == motif[i]))
        {
            if(isupper((*prec)->lettre))
            {
                maj = 1;
            }
            else
            {
                maj = 0;
            }
            prec = &((*prec)->fils);
            i++;
        }
        else
        {   
            (*prec) = NULL;
            continuer = 0;
        }
    }

    if(maj)
    {
        printf("%s\n",motif);
    }

    if((*prec) != NULL)
    {
        Affichage(prec,motif,i);
    }
}