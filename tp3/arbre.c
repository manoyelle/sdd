#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "arbre.h"

void LibererArbre(cellule_t **tete) 
{
    cellule_t        * cour = *tete;

    if (cour != NULL)
    {
        LibererArbre(&(cour->frere));
        LibererArbre(&(cour->fils));
        free(cour);
    }
}

cellule_t * AllocationMaillon(char l)
{
    cellule_t 		 * elt = (cellule_t*) malloc(sizeof(cellule_t));
    if (elt != NULL)
    {
        elt->lettre = l;
        elt->frere = NULL;
        elt->fils = NULL;
    }
    return elt;
}

cellule_t ** rechPrecTrie(cellule_t **tete, char c) 
{
    cellule_t 		 * cour = *tete;
    cellule_t 		** prec = tete;

    while((cour != NULL)&&((tolower(cour->lettre)) < c))
    {
    	prec = &cour->frere;
        cour = cour->frere;
    }
    return prec;
}