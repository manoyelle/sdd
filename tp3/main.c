#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "pile.h"
#include "arbre.h"
#include "probleme.h"

int main(int argc, char *argv[])
{
    FILE             * fichier = NULL;
    cellule_t        * tete = NULL, * tete1 = NULL, * tete2 = NULL, *tete3 = NULL;
    char               c[30]="";
    int                i = 0;

    if(argc >= 2)
    {
        if(argc == 3)
        {
            strncpy(c,argv[2],29);
        }

        fichier = fopen(argv[1], "r");
        if(fichier != NULL)
        {
            Affichage(&tete,c,i);
            tete = Construction(fichier);
            RechercheMotif(&tete, c);
            LibererArbre(&tete);
            fclose(fichier);
        }
    }
    else
    {
        /*-----------------------*/
        /* Tests de construction */
        /*-----------------------*/
            // Test de construction d'un arbre vide
            fichier = fopen("testvide.txt","r");
            if(fichier != NULL)
            {
                tete = Construction(fichier);
                fclose(fichier);
            }

            // Test de construction avec un seul arbre
            fichier = fopen("test1.txt","r");
            if(fichier != NULL)
            {
                tete1 = Construction(fichier);
                fclose(fichier);
            }

            // Test de construction avec plusieurs arbres
            fichier = fopen("test2.txt","r");
            if(fichier != NULL)
            {
                tete2 = Construction(fichier);
                fclose(fichier);
            }
        
        /*-------------------*/
        /* Tests d'affichage */
        /*-------------------*/
            // Test affichage d'un arbre vide
            printf("Test affichage d'un arbre vide\n");
            Affichage(&tete,c,i);
            printf("\n");

            // Test affichage d'un arbre non vide
            printf("Test affichage d'un arbre non vide\n");
            Affichage(&tete1,c,i);
            printf("\n");

        /*-------------------*/
        /* Tests d'insertion */
        /*-------------------*/
            // Test d'insertion dans un arbre vide
            printf("Test insertion dans un arbre vide\n");
            Insertion(&tete, "chocolat");
            Affichage(&tete,c,i);
            printf("\n");

            // Test d'insertion d'un mot existant
            printf("Test insertion d'un mot existant\n");
            Insertion(&tete2, "abat");
            Affichage(&tete2,c,i);
            printf("\n");

            // Test d'insertion d'un mot dont les lettres sont déjà présentes mais pas le mot
            printf("Test insertion d'un mot dont les lettres sont déjà présentes mais pas le mot\n");
            Insertion(&tete2, "ab");
            Affichage(&tete2,c,i);
            printf("\n");

            // Test d'insertion d'un mot rajoutant des lettres à un mot déjà existant
            printf("Test insertion d'un mot rajoutant des lettres à un mot déjà existant\n");
            Insertion(&tete2, "abattu");
            Affichage(&tete2,c,i);
            printf("\n");

            // Test d'insertion d'un mot vide
            printf("Test insertion d'un mot vide\n");
            Insertion(&tete, "");
            Affichage(&tete,c,i);
            printf("\n");

        /*--------------------*/
        /* Tests de recherche */
        /*--------------------*/
            // Recherche d'un motif dans un arbre vide
            printf("Recherche d'un motif dans un arbre vide -> motif = 'a'\n");
            strcpy(c,"a");
            RechercheMotif(&tete3,c);
            printf("\n");

            // Recherche d'un motif vide dans un arbre
            printf("Recherche d'un motif vide dans un arbre -> motif = ''\n");
            strcpy(c,"");
            RechercheMotif(&tete2,c);
            printf("\n");

            // Recherche d'un motif inexistant dans un arbre
            printf("Recherche d'un motif inexistant dans un arbre -> motif = 'fl'\n");
            strcpy(c,"fl");
            RechercheMotif(&tete2,c);
            printf("\n");

            // Recherche d'un motif présent dans plusieurs mots
            printf("Recherche d'un motif présent dans plusieurs mots -> motif = 'ab'\n");
            strcpy(c,"ab");
            RechercheMotif(&tete2,c);
            printf("\n");

            // Recherche d'un motif qui est un mot présent
            printf("Recherche d'un motif qui est un mot présent -> motif = 'abattu'\n");
            strcpy(c,"abattu");
            RechercheMotif(&tete2,c);
            printf("\n");


        LibererArbre(&tete);
        LibererArbre(&tete1);
        LibererArbre(&tete2);
        LibererArbre(&tete3);
    }

    return 0;
}