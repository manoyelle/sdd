#ifndef ARBRE
#define ARBRE

typedef struct cellule cellule_t;
struct cellule
{
    char               lettre;
    struct cellule   * fils;
    struct cellule   * frere;
};

/*------------------------------------------------------------------------------*/
/*AllocationMaillon     Alloue un maillon et initialise ses champs              */
/*                                                                              */
/*en entée :        c : caractère à mettre dans le champ lettre                 */
/*                                                                              */
/*en sortie :  adresse du maillon si l'alocation c'est bien passée, NULL sinon  */
/*------------------------------------------------------------------------------*/
cellule_t * AllocationMaillon(char l);

/*------------------------------------------------------------------------------*/
/* LibererArbre        Libère l'arbre                                           */
/*                                                                              */
/* en entrée :  tete : adresse de la tete de l'arbre à libérer              */
/*------------------------------------------------------------------------------*/
void LibererArbre(cellule_t **tete);

/*------------------------------------------------------------------------------*/
/* rechPrecTrie        Recherche le précédent de la première occurence de c     */
/*                     dans une liste de frères d'un Arbre                      */
/*                                                                              */
/* en entrée :  tete : adresse de la tete de la Arbre                           */
/*                 c : caractère recherché                                      */
/*                                                                              */
/* en sortie :  l'adresse du champ frère du noeud précédent                     */
/*              le noeud contenant c ou une lettre supérieure                   */
/*------------------------------------------------------------------------------*/
cellule_t ** rechPrecTrie(cellule_t **tete, char c);

#endif