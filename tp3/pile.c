#include <stdio.h>
#include <stdlib.h>
#include "pile.h"

void libererPile(pileTete_t ** maPile)
{
    free((*maPile)->base);
    free(*maPile);
    maPile = NULL;
}

pileTete_t * initPile(int taille)
{
    pileTete_t           * maPile = (pileTete_t*)malloc(sizeof(pileTete_t));

    if(maPile != NULL)
    {
        maPile->taille = taille;
        maPile->sommet = -1;
        maPile->base = (pile_t*)malloc(sizeof(pile_t) * taille);
        if(maPile->base == NULL)
        {
            libererPile(&maPile);
            maPile = NULL;
        }
    }

    return maPile;
}

int pileVide(pileTete_t * maPile)
{
    return (maPile->sommet == -1);
}

int pilePleine(pileTete_t * maPile)
{
    return (maPile->sommet == (maPile->taille - 1));
}

int sommet(pileTete_t * maPile, pile_t * adrV)
{
    int                etat = 1;

    if(!pileVide(maPile))
    {
        *adrV = *(maPile->base + maPile->sommet);
        etat = 0;
    }

    return etat;
}

int empiler(pileTete_t ** maPile, pile_t v)
{
    int                etat = 1;

    if(!pilePleine(*maPile))
    {
        (*maPile)->sommet = (*maPile)->sommet + 1; 
        *((*maPile)->base + (*maPile)->sommet) = v;
        etat = 0;
    }

    return etat;
}

int depiler(pileTete_t ** maPile, pile_t * adrV)
{
    int                etat = 1;

    if(!pileVide(*maPile))
    {
        *adrV = *((*maPile)->base + (*maPile)->sommet);
        (*maPile)->sommet = (*maPile)->sommet - 1; 
        etat = 0;
    }

    return etat;
}



