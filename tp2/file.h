#ifndef FILE
#define FILE

typedef int file_t;

typedef struct fileTete fileTete_t;
struct fileTete
{
    int                taille, quantite, rgdeb, rgfin;
    file_t           * base;
};

/*---------------------------------------------------------------*/
/* libererFile         libère la file et le bloc tete de la file */
/*                                                               */
/* entrée :   maFile : Pointeur de pointeur sur fileTete_t       */
/*---------------------------------------------------------------*/
void libererFile(fileTete_t ** maFile);

/*-----------------------------------------------------------------------------------------*/
/* initFile            initialise le bloc tete de la file et la file avec une taille donné */
/*                                                                                         */
/* entrée :   taille : Entier donnant la taille de la file                                 */
/*                                                                                         */
/* sortie : Retourne le bloc tete de la file, pointeur fileTete_t                          */
/*-----------------------------------------------------------------------------------------*/
fileTete_t * initFile(int taille);

/*--------------------------------------------------------------*/
/* fileVide            vérifie si la file est file est vide     */
/*                                                              */
/* entrée :   maFile : Pointeur sur fileTete                    */
/*                                                              */
/* sortie : Retourne vrai(1) si la file est vide, faux(0) sinon */
/*--------------------------------------------------------------*/
int fileVide(fileTete_t * maFile);

/*----------------------------------------------------------------*/
/* filePleine          vérifie si la file est file est pleine     */
/*                                                                */
/* entrée :   maFile : Pointeur sur fileTete                      */
/*                                                                */
/* sortie : Retourne vrai(1) si la file est pleine, faux(0) sinon */
/*----------------------------------------------------------------*/
int filePleine(fileTete_t * maFile);

/*----------------------------------------------------------------------*/
/* enfiler             ajoute un élément à la file                      */
/*                                                                      */
/* entrée :   maFile : Pointeur de pointeur sur fileTete                */
/*            valeur : élément ajouté à la file, de type file_t         */
/*                                                                      */
/* sortie : Retourne vrai(1) si l'ajout c'est bien passé, faux(0) sinon */
/*----------------------------------------------------------------------*/
int enfiler(fileTete_t ** maFile, file_t valeur);

/*---------------------------------------------------------------------------------------*/
/* defiler             supprime un élément à la file                                     */
/*                                                                                       */
/* entrée :   maFile : Pointeur de pointeur sur fileTete                                 */
/*                 v : pointeur de type file_t recevant la valeur de l'élément supprimer */
/*                                                                                       */
/* sortie : Retourne vrai(1) si la suppression c'est bien passé, faux(0) sinon           */
/*---------------------------------------------------------------------------------------*/
int defiler(fileTete_t ** maFile, file_t * v);

#endif