#include <stdio.h>
#include <stdlib.h>
#include "file.h"

void libererFile(fileTete_t ** maFile)
{
    free((*maFile)->base);
    free(*maFile);
    maFile = NULL;
}

fileTete_t * initFile(int taille)
{
    fileTete_t       * maFile = (fileTete_t*)malloc(sizeof(fileTete_t));

    if(maFile != NULL)
    {
        maFile->base = (file_t*)malloc(sizeof(file_t) * taille);
        maFile->taille = taille;
        maFile->quantite = 0;
        maFile->rgdeb = 0;
        maFile->rgfin = 0;
        if(maFile->base == NULL)
        {
            libererFile(&maFile);
            maFile = NULL;
        }
    }

    return maFile;
}

int fileVide(fileTete_t * maFile)
{
    return (maFile->quantite == 0);
}

int filePleine(fileTete_t * maFile)
{
    return (maFile->quantite == maFile->taille);
}

int enfiler(fileTete_t ** maFile, file_t valeur)
{
    int                etat = 1;

    if(!filePleine(*maFile))
    {
        (*maFile)->rgfin = ((*maFile)->rgfin + 1) % (*maFile)->taille;
        *((*maFile)->base + (*maFile)->rgfin) = valeur;
        (*maFile)->quantite += 1;
        etat = 0;
    }

    return etat;
}

int defiler(fileTete_t ** maFile, file_t * v)
{
    int                etat = 1;

    if(!fileVide(*maFile))
    {
        *v = *((*maFile)->base + (*maFile)->rgdeb);
        (*maFile)->rgdeb = ((*maFile)->rgdeb + 1) % (*maFile)->taille;
        (*maFile)->quantite -= 1;
        etat = 0;
    }

    return etat;
}



