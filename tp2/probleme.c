#include <stdio.h>
#include "probleme.h"
#include "pile.h"
#include "file.h"

int cnp_rec(int n, int p, int * ok)
{	
	int res = 1;
	
	if (n >= 0 && p <= n && p >= 0)
	{
		if (n!=p && p!=0)
		{
			res = cnp_rec(n-1,p,ok);
			if (ok)
			{
				res = res + cnp_rec(n-1,p-1,ok);
			}
		}
	}
	else
	{
		*ok = 0;
	}
	return res;
}

int cnp_iter(int n, int p, int * ok)
{
	pileTete_t * tete_pile = initPile(MAX);
	int fin = 0, res = 0;
	pile_t ni = n, pi = p;
	*ok = 1;
	if (n >= 0 && p <= n && p >= 0) 
	{
		while (!fin) 
		{
			while (ni != pi && pi != 0)
			{
				empiler(&tete_pile, ni-1);
				empiler(&tete_pile, pi);
				ni = ni - 1;
				pi = pi - 1;
			}
			res = res + 1;
			if (!pileVide(tete_pile))
			{
				if (depiler(&tete_pile, &pi) == 1 || depiler(&tete_pile, &ni) == 1)
				{
					fin = 1;
					*ok = 0;
				}
			}
			else 
			{
				fin = 1;
			}
		}
	}
	else
	{
		*ok = 0;
	}
	libererPile(&tete_pile);
	return res;
}

void testPile()
{
	pileTete_t 		 * maPile = NULL;
	pile_t 			   val = 0;
	int 			   etat = 0;

	maPile = initPile(5);
	printf("la pile est de taille -> %d\n",(maPile)->taille);
	if(maPile != NULL)
	{
		for(int i=1; i <= maPile->taille + 1; i++)
		{
			printf("On essaye d'empiler l'élément %d : ",i);
			etat = empiler(&maPile, val);
			if(etat == 0)
			{
				printf("succes\n");
			}
			else
			{
				printf("erreur\n");
			}
		}

		printf("\n");

		for(int i=maPile->sommet + 1; i >= 0; i--)
		{
			printf("On essaye de depiler l'élément %d : ",i);
			etat = depiler(&maPile, &val);
			if(etat == 0)
			{
				printf("succes\n");
			}
			else
			{
				printf("erreur\n");
			}
		}

		libererPile(&maPile);
	}
    printf("\n");
}

void testFile()
{
    fileTete_t       * maFile = NULL;
    file_t             val = 0;
    int                etat = 0;

    maFile = initFile(5);
    printf("la file est de taille -> %d\n",(maFile)->taille);
    if(maFile != NULL)
    {
        for(int i=1; i <= maFile->taille + 1; i++)
        {
            printf("On essaye d'enfiler l'élément %d : ",i);
            etat = enfiler(&maFile, val);
            if(etat == 0)
            {
                printf("succes\n");
            }
            else
            {
                printf("erreur\n");
            }
        }

        printf("\n");

        for(int i=maFile->quantite; i >= 0; i--)
        {
            printf("On essaye de defiler l'élément %d : ",i);
            etat = defiler(&maFile, &val);
            if(etat == 0)
            {
                printf("succes\n");
            }
            else
            {
                printf("erreur\n");
            }
        }

        libererFile(&maFile);
    }
    printf("\n");
}