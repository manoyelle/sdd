#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "pile.h"
#include "file.h"
#include "probleme.h"

int main(int argc, char ** argv)
{
	int 			   n = 0,p = 0, OK = 1, res = 0;
	float 			   temps1 = 0, temps2 = 0;
	clock_t 		   t1, t2;
	if(argc == 3)
	{
		n = atoi(argv[1]);
		p = atoi(argv[2]);

		testPile();
        testFile();

		t1 = clock();
		res = cnp_rec(n,p,&OK);
		if(OK == 1)
		{
			printf("cnp_rec(%d,%d) = %d\n", n, p, res);
			t2 = clock();
			temps1 = (float)(t2-t1)/CLOCKS_PER_SEC;
			printf("CNP recursif temps = %f secondes\n", temps1);

			t1 = 0; t2 = 0;

			t1 = clock();
			res = cnp_iter(n,p,&OK);
			if(OK == 1)
			{
				printf("cnp_iter(%d,%d) = %d\n", n, p, res);
				t2 = clock();
				temps2 = (float)(t2-t1)/CLOCKS_PER_SEC;
				printf("CNP iteratif temps = %f secondes\n", temps2);

				if(temps1 < temps2)
				{
					printf("la fonction CNP recursive est plus rapide\n");
				}
				else
				{
					printf("la fonction CNP iterative est plus rapide\n");
				}
			}

		}
		
	}
	return 0;
}