#ifndef PILE
#define PILE
#define FORMAT %d
#define MAX 1024

typedef int pile_t;

typedef struct pileTete pileTete_t;
struct pileTete
{
    int                taille, sommet;
    pile_t           * base;
};

/*---------------------------------------------------------------*/
/* libererPile         libère la pile et le bloc tete de la pile */
/*                                                               */
/* entrée :   maPile : Pointeur de pointeur sur pileTete_t       */
/*---------------------------------------------------------------*/
void libererPile(pileTete_t ** pile);

/*-----------------------------------------------------------------------------------------*/
/* initPile            initialise le bloc tete de la pile et la pile avec une taille donné */
/*                                                                                         */
/* entrée :   taille : Entier donnant la taille de la pile                                 */
/*                                                                                         */
/* sortie : Retourne le bloc tete de la pile, pointeur pileTete_t                          */
/*-----------------------------------------------------------------------------------------*/
pileTete_t * initPile(int taille);

/*--------------------------------------------------------------*/
/* pileVide            vérifie si la pile est pile est vide     */
/*                                                              */
/* entrée :   maPile : Pointeur sur pileTete                    */
/*                                                              */
/* sortie : Retourne vrai(1) si la pile est vide, faux(0) sinon */
/*--------------------------------------------------------------*/
int pileVide(pileTete_t * pile);

/*----------------------------------------------------------------*/
/* pilePleine          vérifie si la pile est pile est pleine     */
/*                                                                */
/* entrée :   maPile : Pointeur sur pileTete                      */
/*                                                                */
/* sortie : Retourne vrai(1) si la pile est pleine, faux(0) sinon */
/*----------------------------------------------------------------*/
int pilePleine(pileTete_t * pile);

/*------------------------------------------------------------------------------------*/
/* sommet              donne le sommet de la pile                                     */
/*                                                                                    */
/* entrée :   maPile : Pointeur sur pileTete                                          */
/*              adrV : Pointeur sur pile_t, récupérant l'adresse du sommet de la pile */
/*                                                                                    */
/* sortie : Retourne vrai(1) si le sommet existe, faux(0) sinon                       */
/*------------------------------------------------------------------------------------*/
int sommet(pileTete_t * maPile, pile_t * adrV);

/*----------------------------------------------------------------------*/
/* empiler             ajoute un élément à la pile                      */
/*                                                                      */
/* entrée :   maPile : Pointeur de pointeur sur pileTete                */
/*            valeur : élément ajouté à la pile, de type pile_t         */
/*                                                                      */
/* sortie : Retourne vrai(1) si l'ajout c'est bien passé, faux(0) sinon */
/*----------------------------------------------------------------------*/
int empiler(pileTete_t ** maPile, pile_t v);

/*---------------------------------------------------------------------------------------*/
/* depiler             supprime un élément à la pile                                     */
/*                                                                                       */
/* entrée :   maPile : Pointeur de pointeur sur pileTete                                 */
/*                 v : Pointeur de type pile_t recevant la valeur de l'élément supprimer */
/*                                                                                       */
/* sortie : Retourne vrai(1) si la suppression c'est bien passé, faux(0) sinon           */
/*---------------------------------------------------------------------------------------*/
int depiler(pileTete_t ** maPile, pile_t * adrV);

#endif