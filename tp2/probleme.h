/*-----------------------------------------------------------------------------------------*/
/* cnp_rec             fonction CNP récursive                                              */
/*                                                                                         */
/* entrée :     n : entier supérieur à 0                                                   */
/*              p : entier supérieur à 0                                                   */
/*             ok : pointeur du code retour (1 si ok, 0 sinon)                             */
/*                                                                                         */
/* sortie :    CNP(n-1,p) + CNP(n-1,p-1)                                                   */
/*-----------------------------------------------------------------------------------------*/
int cnp_rec(int n, int p, int * ok);

/*-----------------------------------------------------------------------------------------*/
/* cnp_iter            fonction CNP itérative                                              */
/*                                                                                         */
/* entrée :     n : entier supérieur à 0                                                   */
/*              p : entier supérieur à 0                                                   */
/*             ok : pointeur du code retour (1 si ok, 0 sinon)                             */
/*                                                                                         */
/* sortie :   CNP(n-1,p) + CNP(n-1,p-1)                                                    */
/*-----------------------------------------------------------------------------------------*/
int cnp_iter(int n, int p, int * ok);

void testPile();